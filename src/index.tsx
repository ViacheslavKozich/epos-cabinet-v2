import React from 'react';
import ReactDOM from 'react-dom';
import axios from 'axios';

import { globalAppConfig, globalAppConfigUrl } from './appConfig';
import { cabinetAxios } from 'apis/cabinetApi/cabinetAxios';
import { identityAxios } from 'apis/identityServer/identityAxios';

import { App } from './app/App';

import './i18n/i18n';
import './index.scss';


// PUBLIC_URL задается через cross-env в scripts
const { PUBLIC_URL } = process.env;


axios.get(`${PUBLIC_URL}/${globalAppConfigUrl}`)
	.then((response) => {
		globalAppConfig.appConfig = response.data;
		const { API_URL, AUTH_URL } = globalAppConfig.appConfig;
		cabinetAxios.setBaseUrl(API_URL);
		identityAxios.setBaseUrl(AUTH_URL);
	})
	.catch((error) => {
		console.error('Error: appConfig.json didn\'t read correctly. DefaultAppConfig will be applied.');
	})
	.then(() => {
		ReactDOM.render(
			<React.StrictMode>
				<App />
			</React.StrictMode>,
			document.getElementById('root')
		);
	});
