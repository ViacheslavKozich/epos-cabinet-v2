import { TypedUseSelectorHook, useDispatch, useSelector, shallowEqual } from 'react-redux';
import type { TState, TDispatch } from 'reduxStore/redux-store';

// Use throughout your app instead of plain `useDispatch` and `useSelector`
// from https://redux.js.org/usage/usage-with-typescript
const useAppDispatch = () => useDispatch<TDispatch>();
const useAppSelector: TypedUseSelectorHook<TState> = useSelector;

export { useAppDispatch, useAppSelector, shallowEqual };
