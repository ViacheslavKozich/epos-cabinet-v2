import { useMemo, useCallback } from 'react';
import { useAppDispatch, useAppSelector, shallowEqual } from 'hooks';
import { useAuthContext } from 'app/providers';
import { identityApiCalls } from 'apis/identityServer/identityApiCalls';
import { tokensService } from 'app/auth/tokens/tokensService';
import { decodeTokenClaims } from 'utils';
// Types
import { MutableRefObject } from 'react';
import { UserInfo } from 'apis/cabinetApi/epos_cabinet_api/models';


/**
 * @summary Авторизованный пользователь и действия авторизации
 * @property `userInfo` Профиль авторизованного через iii пользователя, полученный с cabinet api
 * @property `isUserAuth` При false доступ только к /auth, при true доступ к приложению
 * @property `postAuthUrl` Адрес редиректа после прохождения авторизации из authContext
 * @property `logInUser` Редирект пользователя на iii для авторизации и получения authCode
 * @property `setUserAuth` Обмен authCode на токены через cabinet api, получение профиля пользователя, запись в стор
 * @property `logOutUser` Удаление токенов и редирект на logout url iii
*/
interface IUseAuthReturn {
	/** Профиль авторизованного через iii пользователя, полученный с cabinet api */
	readonly userInfo: UserInfo | null;
	/** При false доступ только к /auth, при true доступ к приложению */
	readonly isUserAuth: boolean;
	/** Адрес редиректа после прохождения авторизации из authContext */
	readonly postAuthUrl: MutableRefObject<string>;
	/** Редирект пользователя на iii для авторизации и получения authCode */
	readonly logInUser: (fromUrl: string) => void;
	/** Обмен authCode на токены через cabinet api, получение профиля пользователя, запись в стор */
	readonly authorizeUser: (authCode: string) => Promise<void>;
	/** Удаление токенов и редирект на logout url iii */
	readonly logOutUser: VoidFunction;
};

const useAuth = (): IUseAuthReturn => {
	const { userInfo, idToken } = useAppSelector((state) => state.auth, shallowEqual);

	const { isUserAuth, postAuthUrl } = useAuthContext();

	const logInUser = useCallback((fromUrl: string): void => {
		// Редирект на iii для авторизации. fromUrl передадим в params.state для получения в хэше
		identityApiCalls.redirectToAuthServerForLogin(fromUrl);
	}, []);

	const authorizeUser = useCallback(async (authCode: string): Promise<void> => {
		// Получить на api.identity токены, задать токены в state, session storage
		await tokensService.authorizeUserInApp(authCode);
	}, []);

	const logOutUser = useCallback((): void => {
		// Удалить токены из local и session storage
		tokensService.removeTokens();
		// Редирект на iii для логаута
		identityApiCalls.redirectToAuthServerForLogout(idToken);
	}, []);

	return {
		userInfo,
		isUserAuth,
		postAuthUrl,
		logInUser,
		authorizeUser,
		logOutUser,
	};
}

export { useAuth };
