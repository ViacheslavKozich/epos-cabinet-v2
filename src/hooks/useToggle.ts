import { useState, useCallback } from 'react';


type TUseToggleReturn = [boolean, VoidFunction];

/**
 * Hook для создания функции-переключателя на основе setState
 * @param {boolean} initialState начальное значение
 * @returns кортеж, как в setState
 */
const useToggle = (initialState: boolean = false): TUseToggleReturn => {
	// Инициализируем стейт
	const [state, setState] = useState<boolean>(initialState);

	// Меморизируем функцию, инвертирующую булевое значение
	const togglerFunction = useCallback((): void => {
		setState((state) => !state);
	}, []);

	// Возвращаем кортеж
	return [state, togglerFunction]
}

export { useToggle };
