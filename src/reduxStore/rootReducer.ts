import { combineReducers } from 'redux';

import { authSlice } from 'app/auth/_redux/authSlice';


const rootReducer = combineReducers({
	// persisted reducer, сохраняется в local storage
	auth:    authSlice.reducer,
	// остальные редюсеры
});

export { rootReducer };
