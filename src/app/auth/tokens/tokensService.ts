import axios from 'axios';
import { IIIApi } from 'apis/cabinetApi/epos_cabinet_api';
import { globalAppConfig } from 'appConfig';
import { sessionStorageHelper } from 'utils';
import { store } from 'reduxStore/redux-store';
import { authSlice } from '../_redux/authSlice';
// Types
import { ITokenResponse } from '../authTypes';


class AuthTokensService {
	constructor() {
		this.authorizeUserInApp = this.authorizeUserInApp.bind(this);
		this.updateTokensAfterAccessTokenExpEnd = this.updateTokensAfterAccessTokenExpEnd.bind(this)
	}

	/**
	 * Метод делает запрос к IIIApi на получение токенов и обновляет их во всех хранилищах
	 * @param {string} authCode код авторизации, полученный в хэше redirectUrl после авторизации на iii
	 */
	async authorizeUserInApp(authCode: string): Promise<void> {
		const tokens = await this.getTokensWithAuthCode(authCode);
		this.setTokens(tokens);
	}

	/**
	 * Метод делает запрос к IIIApi на получение токенов и обновляет их во всех хранилищах
	 */
	async updateTokensAfterAccessTokenExpEnd(): Promise<void> {
		const { refreshToken } = store.getState().auth;
		const tokens = await this.getTokensWithRefreshToken(refreshToken); 
		this.setTokens(tokens);
	}

	//! ========== III API ==========
	/**[ GET  ] /iii/accesstoken/authcode/{authcode}         | Получение токенов доступа по коду авторизации
	 * @param {string} authCode код авторизации, полученный в хэше redirectUrl после авторизации на iii
	 */
	private async getTokensWithAuthCode(authCode: string): Promise<ITokenResponse> {
		const axiosForClient = axios.create({
			baseURL: this.getDynamicBaseURL(),
			headers: { 'Content-Type': 'application/json' }
		});
		const iiiApiClient = new IIIApi(undefined, undefined, axiosForClient);
		const testMode = (window.location.hostname === 'localhost') ? true : false;
		try {
			const axiosResponse = await iiiApiClient.v1iiiaccesstokenauthcodeauthcodeIIIGET(authCode, testMode);
			return axiosResponse.data;
		} catch(error) {
			console.log(error)
			debugger
			// убрать токены и разлогинить?
			return Promise.reject(error);
		}
	}

	/**[ GET  ] /iii/accesstoken/refreshtoken/{refreshtoken} | Получение токенов доступа по refresh-токену
	 * @param {string} refreshToken refreshToken из local storage
	 */
	private async getTokensWithRefreshToken(refreshToken: string): Promise<ITokenResponse> {
		const axiosForClient = axios.create({
			baseURL: this.getDynamicBaseURL(),
			headers: { 'Content-Type': 'application/json' }
		});
		const iiiApiClient = new IIIApi(undefined, undefined, axiosForClient);
		try {
			const axiosResponse = await iiiApiClient.v1iiiaccesstokenrefreshtokenrefreshtokenIIIGET(refreshToken);
			return axiosResponse.data;
		} catch(error) {
			console.log(error)
			debugger
			// убрать токены и разлогинить?
			return Promise.reject(error);
		}
	}

	/**
	 * Метод сохраняет в хранилищах приложения полученные токены:
	 * в `session storage` записывает accessToken для установки в headers запросов к api;
	 * в `app.reducer` диспатчит accessToken для переключения состояний приложения;
	 * в persisted `auth.reducer` диспатчит refreshToken и idToken для сохранения вне сессии
	 * @param {ITokenResponse} tokens полученные с api токены
	 */
	private setTokens(tokens: ITokenResponse): void {
		// debugger
		this.setAccessTokenLocal(tokens.accessToken);
		store.dispatch(authSlice.actions.tokensFetched(tokens));
	}

	/**
	 * Метод удаляет токены перед логаутом пользователя
	 */
	removeTokens(): void {
		sessionStorageHelper.removeAccessToken();
		store.dispatch(authSlice.actions.clearAuthState());
	}

	/**
	 * Метод возвращает локально сохраненный accessToken для axios headers запросов к api
	 * @returns {string} локально сохраненный accessToken
	 */
	getAccessTokenLocal(): string {
		return sessionStorageHelper.getAccessToken();
	}

	/**
	 * Метод локально сохраняет accessToken для последующего использования в axios headers
	 * @param {string} accessToken актуальный токен, полученный при запросе на identity
	 */
	private setAccessTokenLocal(accessToken: string): void {
		sessionStorageHelper.setAccessToken(accessToken);
	}

	private getDynamicBaseURL(): string {
		return globalAppConfig.appConfig.API_URL;
	}

}

const tokensService = new AuthTokensService();

export { tokensService };
