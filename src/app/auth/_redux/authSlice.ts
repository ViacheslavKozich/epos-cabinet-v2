import { createSlice, PayloadAction } from '@reduxjs/toolkit';
// Types
import { IUserInfo, ITokenResponse } from '../authTypes';


//* persisted reducer, сохраняется между сессиями в local storage. accessToken в session storage
interface IAuthInitialState {
	userInfo:     IUserInfo | null;
	refreshToken: string;
	idToken:      string;
}
const authInitialState: IAuthInitialState = {
	userInfo:     null,
	refreshToken: '',
	idToken:      '',
};

const authSlice = createSlice({
	name: 'auth',
	initialState: authInitialState,
	reducers: {
		//* getTokensWithAuthCode / getTokensWithRefreshToken {status: ok}
		tokensFetched: (state, action: PayloadAction<ITokenResponse>) => {
			const { refreshToken, idToken } = action.payload;
			state.refreshToken = refreshToken;
			state.idToken = idToken;
		},
		clearAuthState: (state) => {
			state.refreshToken = '';
			state.idToken = '';
			state.userInfo = null;
		},

		//* getUserData {status: ok}
		userInfoFetched: (state, action: PayloadAction<IUserInfo>) => {
			const fetchedUser = action.payload;
			state.userInfo = fetchedUser;
		},

	}
});

export { authSlice };
