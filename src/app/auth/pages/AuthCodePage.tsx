import React, { useEffect } from 'react';
import { useLocation } from 'react-router-dom';
import { useAuth } from 'hooks';
import { getQueryStringParams } from 'utils';
// Types


const AuthCodePage: React.FC = () => {
	const { hash } = useLocation();
	const { authorizeUser, postAuthUrl } = useAuth();

	// Перенаправлены с iii и в хеше authCode
	useEffect(() => {
		if (hash) {
			const params = getQueryStringParams(hash);
			const authCode = params.code;
			const fromUrl = params.state;
			postAuthUrl.current = fromUrl;
			authorizeUser(authCode);
		}
	}, [hash]);

	return null;
}

export { AuthCodePage };
