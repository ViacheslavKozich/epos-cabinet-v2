import React, { createContext, useState, useLayoutEffect, useRef, useCallback, useContext } from 'react';
import { useAppSelector, shallowEqual } from 'hooks';
import { sessionStorageHelper } from 'utils';
// Types


interface IAuthContext {
	/** При false доступ только к /auth, при true доступ к приложению */
	readonly isUserAuth:    boolean;
	/** Адрес редиректа после прохождения авторизации */
	readonly postAuthUrl:   React.MutableRefObject<string>;
};
// @ts-ignores
const AuthContext = createContext<IAuthContext>();
const useAuthContext = () => useContext(AuthContext);


const AuthProvider: React.FC = ({ children }) => {
	const postAuthUrl = useRef<string>('/');

	const accessToken = sessionStorageHelper.getAccessToken();
	const { refreshToken } = useAppSelector((state) => state.auth, shallowEqual);

	//* isUserAuth для перевода приложения между состояниями
	const [isUserAuth, setUserAuth] = useState<boolean>(Boolean(accessToken && refreshToken));
	useLayoutEffect(() => {
		if (accessToken && !isUserAuth) {
			setUserAuth(true);
		}
	}, [accessToken, refreshToken]);


	const value = {
		isUserAuth,
		postAuthUrl,
	};

	return (
		<AuthContext.Provider value={value}>
			{children}
		</AuthContext.Provider>
	);
}

export { AuthProvider, useAuthContext };
