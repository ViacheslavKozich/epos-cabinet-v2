import React from 'react';
import { Navigate, useLocation } from 'react-router-dom';

import { useAuth } from 'hooks';


const RequiredAuthHOC: React.FC = ({ children }) => {
	const { isUserAuth } = useAuth();
	const { pathname } = useLocation();
	
	// Если пользователь не авторизован - редирект на страницу авторизации
	return (isUserAuth) ? (
		<>{children}</>
	) : (
		// Текущий pathname передадим как fromUrl в LoginRedirectURL
		// для редиректа после авторизации на iii
		<Navigate to="auth/login" state={pathname} />
	);
}

export { RequiredAuthHOC };
