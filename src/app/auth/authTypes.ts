import { MutableRefObject } from 'react';
import { UserInfo, TokenResponse } from 'apis/cabinetApi/epos_cabinet_api/models';


/**
 * @summary Данные пользователя
 */
export interface IUserInfo extends UserInfo {};

export interface ITokenResponse extends TokenResponse {}

/**
 * @summary redirect url после авторизации на iii
 */
export interface IPostAuthUrl {
	postAuthUrl: MutableRefObject<string>
};
