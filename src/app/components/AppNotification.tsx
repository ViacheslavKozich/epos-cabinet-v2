import React, { useCallback } from 'react';
import { useSnackbar, SnackbarContent } from 'notistack';
import {
	Stack,
	Typography, } from '@mui/material';
// Types
import { SnackbarKey, SnackbarMessage } from 'notistack';


interface IAppNotificationProps {
	id:      SnackbarKey;
	message: SnackbarMessage;
}
const AppNotification = React.forwardRef<HTMLDivElement, IAppNotificationProps>(({ id, message }, ref) => {
	const { closeSnackbar } = useSnackbar();

	const handleDismiss = useCallback(() => {
		closeSnackbar(id);
	}, [id, closeSnackbar]);

	return (
		<SnackbarContent ref={ref}>
{/* ref={ref} */}

		<Stack
			// component={SnackbarContent}
			// ref={ref}
			alignItems="center"
			sx={(theme) => ({
				width: '100vw',
				minWidth: '100vw',
				maxWidth: '100vw',
				padding: theme.padding.x2,
				color: theme.color.bg_01,
				font: theme.font.body.md,
				background: theme.color.epos.notification
			})}
		>
			<Typography variant="subtitle2">{message}</Typography>
		</Stack>
		</SnackbarContent>
	);
});

export { AppNotification };
