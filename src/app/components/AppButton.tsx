import React from 'react';
import { ButtonUnstyled, ButtonUnstyledProps } from '@mui/base';
import { muiEsasStyled } from 'app/theme';
import { SxProps, Theme } from '@mui/material/styles';


interface IAppButtonProps extends ButtonUnstyledProps {
	variant?: 'main' | 'icon' | 'text';
	fullWidth?: boolean;
	sx?: SxProps<Theme>;
};
const AppButton: React.FC<IAppButtonProps> = muiEsasStyled(ButtonUnstyled, {
	// Перечисляем пропсы, которые не будут отображаться атрибутами в DOM
	shouldForwardProp: (prop) => (
		prop !== 'variant' && prop !== 'fullWidth' && prop !== 'sx'
	),
	name: 'AppButton'
})<IAppButtonProps>((props) => {
	const {
		theme,
		variant = 'main',
		fullWidth = false,
		disabled = false,
	} = props;

	// Определяем стили в зависимости от пропсов для [sx] 
	const general_sx = {
		position: 'relative',
		display: 'inline-flex',
		justifyContent: 'center',
		alignItems: 'center',
		font: theme.font.small.md,
		borderRadius: theme.radius.x4,
		transition: 'background 0.3s, color 0.3s',
	};

	const sx_variant: any = {
		'main': {
			padding: `${theme.padding.x3} ${theme.padding.x6}`,
			color: theme.color.bg_01,
			border: 'none',
		},
		'icon': {
			padding: `${parseInt(`${theme.padding.x3}`) - 1}px ${parseInt(`${theme.padding.x4}`) - 1}px`,
			background: theme.color.bg_01,
			border: theme.border.primary,
			borderColor: theme.color.tertiary,
		},
		'text': {
			padding: `${theme.padding.x3} ${theme.padding.x4}`,
			border: 'none',
		}
	};

	const sx_disabled: any = {
		'false': {
			cursor: 'pointer',
		},
		'true': {
			cursor: 'not-allowed',
		}
	};

	const sx_variant_disabled: any = {
		'main_false': {
			background: theme.color.epos.primary,
			'&:hover': {
				background: theme.color.epos.hover,
			},
			'&:active': {
				background: theme.color.epos.click,
			}
		},
		'main_true': {
			background: theme.color.epos.notification,
		},

		'icon_false': {
			color: theme.color.primary,
			'&:hover': {
				color: theme.color.epos.hover,
			},
			'&:active': {
				color: theme.color.epos.click,
			},
		},
		'icon_true': {
			color: theme.color.secondary,
		},

		'text_false': {
			color: theme.color.epos.primary,
			background: theme.color.bg_01,
			'&:hover': {
				color: theme.color.bg_01,
				background: theme.color.epos.notification,
			},
			'&:active': {
				color: theme.color.bg_01,
				background: theme.color.epos.primary,
			}
		},
		'text_true': {
			color: theme.color.secondary,
			background: theme.color.tertiary,
		},
	};

	const sx_fullWidth: any = {
		'true': {
			width: '100%'
		},
		'false': {},
	};

	return [
		general_sx,
		sx_variant[`${variant}`],
		sx_disabled[`${disabled}`],
		sx_variant_disabled[`${variant}_${disabled}`],
		sx_fullWidth[`${fullWidth}`],
	];
});

export { AppButton };
