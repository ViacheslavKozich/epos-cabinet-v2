import React from 'react';
import { alpha } from '@mui/material';

import {
	AppBar,
	AppContainer,
	Grid,
} from 'app/components';


const HeaderBar: React.FC = ({ children }) => {
	return (
		<AppBar
			component="header"
			position="sticky"
			sx={(theme) => ({
				color: 'inherit',
				background: alpha(`${theme.color.bg_01}`, 0.2),
				backdropFilter: 'blur(6px)',
				borderBottom: `${theme.border.primary} ${theme.color.quaternary}`,
				boxShadow: 'none',
			})}
		>
			<AppContainer>
				<Grid
					container
					wrap="nowrap"
					justifyContent="space-between"
					alignItems="center"
					sx={(theme) => ({
						padding: `${theme.padding.x2} 0`,
					})}
				>
					{children}
				</Grid>
			</AppContainer>
		</AppBar>
	);
}

export { HeaderBar };
