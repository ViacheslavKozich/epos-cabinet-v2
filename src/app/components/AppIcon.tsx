import { ReactElement } from 'react';
import { SvgIcon, SvgIconProps } from '@mui/material';

import { ReactComponent as Down } from 'svg/down.svg';
import { ReactComponent as Next } from 'svg/next.svg';
import { ReactComponent as Up } from 'svg/up.svg';
import { ReactComponent as TriangleDown } from 'svg/triangleDown.svg';

import { ReactComponent as Account } from 'svg/account.svg';
import { ReactComponent as Logotype } from 'svg/logotype.svg';
import { ReactComponent as Language } from 'svg/language.svg';
import { ReactComponent as Done } from 'svg/done.svg';

//* aside-menu
//  Аналитика
import { ReactComponent as Organisation } from 'svg/organisation.svg';
//  Счета
import { ReactComponent as Home } from 'svg/home.svg';
//  Поставщики услуг
import { ReactComponent as Card } from 'svg/card.svg';
//  Пользователи
import { ReactComponent as Users } from 'svg/users.svg';


interface GenericObject {
	[key: string]: () => ReactElement;
};
const icons: GenericObject = {
	down:         () => <Down/>,
	next:         () => <Next/>,
	up:           () => <Up/>,
	triangleDown: () => <TriangleDown/>,
	account:      () => <Account/>,
	logotype:     () => <Logotype/>,
	language:     () => <Language/>,
	done:         () => <Done/>,

	organisation: () => <Organisation/>,
	home:         () => <Home/>,
	card:         () => <Card/>,
	users:        () => <Users/>,
};


// type TIconType = '' | '';
interface IAppIconProps extends SvgIconProps {
	iconType: string;
};
const AppIcon: React.FC<IAppIconProps> = ({ iconType, ...props }) => {
	return <SvgIcon {...props}>{icons[iconType]()}</SvgIcon>;
}

export { AppIcon };

