import { createTheme, alpha, rgbToHex } from '@mui/material/styles';
import { ruRU } from '@mui/material/locale';
// Types
import './themeAugmentation';

const muiEsasTheme = createTheme({
	//* esas design system params
	padding: {
		x1:  '4px',
		x2:  '8px',
		x3:  '12px',
		x4:  '16px',
		x6:  '24px',
		x8:  '32px',
		x10: '40px',
		x12: '48px',
	},

	margin: {
		x1:  '4px',
		x2:  '8px',
		x3:  '12px',
		x4:  '16px',
		x6:  '24px',
		x8:  '32px',
		x10: '40px',
		x12: '48px',
	},

	radius: {
		x1: '2px',
		x2: '4px',
		x4: '8px',
	},

	border: {
		primary: '1px solid',
	},

	depth: {
		primary:   '0 2px  4px 0 rgba(  0,   0,   0, 0.08)',
		secondary: '0 4px 24px 0 rgba(  0,   0,   0, 0.10)',
		tertiary:  '0 8px 24px 0 rgba( 28,  47, 105, 0.16)',	
	},

	font: {
		header: {
			lg: '500 1.5rem "Rubik"',
			md: '400 1.5rem "Rubik"',
		},
		title: {
			lg: '500 1.375rem "Rubik"',
			md: '400 1.375rem "Rubik"',
		},
		body: {
			lg: '500 1.125rem "Rubik"',
			md: '400 1.125rem "Rubik"',
			sm: '500 1rem "Rubik"',
			xs: '400 1rem "Rubik"',
		},
		small: {
			lg: '500 0.875rem "Rubik"',
			md: '400 0.875rem "Rubik"',
			sm: '400 0.75rem "Rubik"',
		}
	},

	color: {
		epos: {
			primary:      rgbToHex('rgba(236,  62,  55, 1.0)'),
			hover:        rgbToHex('rgba(218,  31,  24, 1.0)'),
			click:        rgbToHex('rgba(203,  15,   8, 1.0)'),
			disabled:     rgbToHex('rgba( 11,  31,  53, 0.3)'),
			notification: rgbToHex('rgba(252, 148, 144, 1.0)'),
		},

		primary:        rgbToHex('rgba( 11,  31,  53, 1.0)'),
		secondary:      rgbToHex('rgba( 11,  31,  53, 0.6)'),
		tertiary:       rgbToHex('rgba(217, 225, 238, 1.0)'),
		quaternary:     rgbToHex('rgba(241, 244, 249, 1.0)'),

		bg_01:          rgbToHex('rgba(255, 255, 255, 1.0)'),
		bg_02:          rgbToHex('rgba(248, 250, 255, 1.0)'),

		button: {
			click:        rgbToHex('rgba(174, 187, 209, 1.0)'),
			hover:        rgbToHex('rgba(205, 214, 229, 1.0)'),
		},

		status: {
			error:        rgbToHex('rgba(255,   0,   0, 1.0)'),
			warning:      rgbToHex('rgba(252, 190,  33, 1.0)'),
			success:      rgbToHex('rgba( 97, 204, 101, 1.0)'),
		},
	},

	//* mui standart theme params 
	typography: {
		fontFamily: ['"Rubik"', '"Roboto"', '"Helvetica"', '"Arial"', 'sans-serif'].join(','),
	},

	palette: {
		primary: {
			main:         rgbToHex('rgba(236,  62,  55, 1.0)'),
			light:        rgbToHex('rgba(203,  15,   8, 1.0)'),
			dark:         rgbToHex('rgba(218,  31,  24, 1.0)'),
			contrastText: rgbToHex('rgba(255, 255, 255, 1.0)'),
		},
	},

	breakpoints: {
		values: {
			xs: 0,
			sm: 600,
			md: 900,
			lg: 1200,
			xl: 1400,
		}
	},

	spacing: 4,

	components: {

	},
}, ruRU);

export { muiEsasTheme };
