import { createStyled } from '@mui/system';
import { muiEsasTheme } from './muiEsasTheme';

const muiEsasStyled = createStyled({ defaultTheme: muiEsasTheme });

export { muiEsasStyled };
