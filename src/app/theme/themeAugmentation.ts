declare module '@mui/material/styles' {
	interface Theme {
		padding: {
			x1: React.CSSProperties['paddingTop'],
			x2: React.CSSProperties['paddingTop'],
			x3: React.CSSProperties['paddingTop'],
			x4: React.CSSProperties['paddingTop'],
			x6: React.CSSProperties['paddingTop'],
			x8: React.CSSProperties['paddingTop'],
			x10: React.CSSProperties['paddingTop'],
			x12: React.CSSProperties['paddingTop'],
		};

		margin: {
			x1: React.CSSProperties['marginTop'],
			x2: React.CSSProperties['marginTop'],
			x3: React.CSSProperties['paddingTop'],
			x4: React.CSSProperties['marginTop'],
			x6: React.CSSProperties['marginTop'],
			x8: React.CSSProperties['marginTop'],
			x10: React.CSSProperties['marginTop'],
			x12: React.CSSProperties['marginTop'],
		};

		radius: {
			x1: React.CSSProperties['borderRadius'],
			x2: React.CSSProperties['borderRadius'],
			x4: React.CSSProperties['borderRadius'],
		};

		border: {
			primary: React.CSSProperties['border'],
		};
		
		depth: {
			primary: React.CSSProperties['boxShadow'],
			secondary: React.CSSProperties['boxShadow'],
			tertiary: React.CSSProperties['boxShadow'],
		};

		font: {
			header: {
				lg: React.CSSProperties['font'],
				md: React.CSSProperties['font'],
			},
			title: {
				lg: React.CSSProperties['font'],
				md: React.CSSProperties['font'],
			},
			body: {
				lg: React.CSSProperties['font'],
				md: React.CSSProperties['font'],
				sm: React.CSSProperties['font'],
				xs: React.CSSProperties['font'],
			},
			small: {
				lg: React.CSSProperties['font'],
				md: React.CSSProperties['font'],
				sm: React.CSSProperties['font'],
			}
		};
	
		color: {
			epos: {
				primary: React.CSSProperties['color'],
				hover: React.CSSProperties['color'],
				click: React.CSSProperties['color'],
				disabled: React.CSSProperties['color'],
				notification: React.CSSProperties['color'],
			},

			primary: React.CSSProperties['color'],
			secondary: React.CSSProperties['color'],
			tertiary: React.CSSProperties['color'],
			quaternary: React.CSSProperties['color'],
	
			bg_01: React.CSSProperties['color'],
			bg_02: React.CSSProperties['color'],
	
			button: {
				click: React.CSSProperties['color'],
				hover: React.CSSProperties['color'],
			},

			status: {
				error: React.CSSProperties['color'],
				warning: React.CSSProperties['color'],
				success: React.CSSProperties['color'],
			},
		};

	}

	interface ThemeOptions {
		padding: {
			x1: React.CSSProperties['paddingTop'],
			x2: React.CSSProperties['paddingTop'],
			x3: React.CSSProperties['paddingTop'],
			x4: React.CSSProperties['paddingTop'],
			x6: React.CSSProperties['paddingTop'],
			x8: React.CSSProperties['paddingTop'],
			x10: React.CSSProperties['paddingTop'],
			x12: React.CSSProperties['paddingTop'],
		};

		margin: {
			x1: React.CSSProperties['marginTop'],
			x2: React.CSSProperties['marginTop'],
			x3: React.CSSProperties['paddingTop'],
			x4: React.CSSProperties['marginTop'],
			x6: React.CSSProperties['marginTop'],
			x8: React.CSSProperties['marginTop'],
			x10: React.CSSProperties['marginTop'],
			x12: React.CSSProperties['marginTop'],
		};

		radius: {
			x1: React.CSSProperties['borderRadius'],
			x2: React.CSSProperties['borderRadius'],
			x4: React.CSSProperties['borderRadius'],
		};

		border: {
			primary: React.CSSProperties['border'],
		};
		
		depth: {
			primary: React.CSSProperties['boxShadow'],
			secondary: React.CSSProperties['boxShadow'],
			tertiary: React.CSSProperties['boxShadow'],
		};

		font: {
			header: {
				lg: React.CSSProperties['font'],
				md: React.CSSProperties['font'],
			},
			title: {
				lg: React.CSSProperties['font'],
				md: React.CSSProperties['font'],
			},
			body: {
				lg: React.CSSProperties['font'],
				md: React.CSSProperties['font'],
				sm: React.CSSProperties['font'],
				xs: React.CSSProperties['font'],
			},
			small: {
				lg: React.CSSProperties['font'],
				md: React.CSSProperties['font'],
				sm: React.CSSProperties['font'],
			}
		};
	
		color: {
			epos: {
				primary: React.CSSProperties['color'],
				hover: React.CSSProperties['color'],
				click: React.CSSProperties['color'],
				disabled: React.CSSProperties['color'],
				notification: React.CSSProperties['color'],
			},

			primary: React.CSSProperties['color'],
			secondary: React.CSSProperties['color'],
			tertiary: React.CSSProperties['color'],
			quaternary: React.CSSProperties['color'],
	
			bg_01: React.CSSProperties['color'],
			bg_02: React.CSSProperties['color'],
	
			button: {
				click: React.CSSProperties['color'],
				hover: React.CSSProperties['color'],
			},

			status: {
				error: React.CSSProperties['color'],
				warning: React.CSSProperties['color'],
				success: React.CSSProperties['color'],
			},
		};
	}
}

export {};
