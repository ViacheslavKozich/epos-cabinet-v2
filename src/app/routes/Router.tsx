import React from 'react';
import { Routes, Route } from 'react-router-dom';

import { Layout } from 'app/layout/Layout';
import { Auth } from 'app/auth/Auth';
import { AuthCodePage } from 'app/auth/pages/AuthCodePage';
import { LoginUserPage } from 'app/auth/pages/LoginUserPage';
import { RequiredAuthHOC } from 'app/auth/pages/RequiredAuthHOC';


const HomePage = React.lazy(() =>	import('app/modules/HomePage'));
const Profile = React.lazy(() =>	import('app/modules/profile/Profile'));
const Analytics = React.lazy(() =>	import('app/modules/analytics/Analytics'));
const Accounts = React.lazy(() =>	import('app/modules/accounts/Accounts'));
const Suppliers = React.lazy(() =>	import('app/modules/suppliers/Suppliers'));
const Users = React.lazy(() =>	import('app/modules/users/Users'));

const Error404 = React.lazy(() =>	import('app/modules/errors/pages/Error404'));


const Router: React.FC = () => {
	return (
		<Routes>
			<Route path="/auth" element={<Auth/>}>
				<Route index element={<AuthCodePage/>} />
				<Route path="login" element={<LoginUserPage/>} />
			</Route>

			<Route path="/" element={
				// Если нету токена - редирект на auth/login 
				<RequiredAuthHOC>
					<Layout/>
				</RequiredAuthHOC>
			}>
				<Route index element={<HomePage/>} />
				{/* <Route index element={<Navigate to="profile" replace/>} /> */}
				<Route path="profile" element={<Profile/>} />
				<Route path="analytics" element={<Analytics/>} />
				<Route path="accounts" element={<Accounts/>} />
				<Route path="suppliers" element={<Suppliers/>} />
				<Route path="users" element={<Users/>} />
			</Route>

			<Route path="*" element={<Error404/>} />
		</Routes>
	);
}

export { Router };
