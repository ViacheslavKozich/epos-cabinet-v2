import React from 'react';
import { Link } from 'react-router-dom';

import {
	Grid,
	Stack,
} from '@mui/material';

import { HeaderBar } from 'app/components/headerComponents/HeaderBar';
import { EposLogotype } from 'app/components/headerComponents/EposLogotype';
import { ProfileToggler } from 'app/components/headerComponents/ProfileToggler';


const AppHeader: React.FC = () => {
	return (
		<HeaderBar>
			<Grid item>
				<Link to="/">
					<EposLogotype />
				</Link>
			</Grid>

			<Stack direction="row">
				<ProfileToggler/>
			</Stack>
		</HeaderBar>
	);
}

export { AppHeader };
