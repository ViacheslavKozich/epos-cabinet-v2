import React from 'react'
import { useTranslation } from 'react-i18next';

import {
	Stack,
	Box,
} from '@mui/material';
import {
	AppIcon,
	AppNavLink,
} from 'app/components';


interface IMainMenuListItem {
	to:       string;
	text:     string;
	iconType: string;
};
const mainMenuItemsList: Array<IMainMenuListItem> = [
	{ to: '/analytics', text: 'analytics', iconType: 'organisation' },
	{ to: '/accounts',  text: 'accounts', iconType: 'home' },
	{ to: '/suppliers', text: 'suppliers', iconType: 'card' },
	{ to: '/users',     text: 'users', iconType: 'users' },
];

const AppAsideMenu: React.FC = () => {
	const keyPrefix = 'layout.aside-menu';
	const { t } = useTranslation(undefined, { keyPrefix });

	return (
		<Box component="nav">
			<Stack
				component="ul"
				spacing={4}
			>
				{mainMenuItemsList.map(({ to, text, iconType }) => (
					<Stack
						key={text}
						component="li"
						direction="row"
						alignItems="center"
						spacing={2}
					>
						<AppIcon iconType={iconType}/>
						<AppNavLink to={to}>
							{t(text)}
						</AppNavLink>
					</Stack>
				))}
			</Stack>
		</Box>
	);
}

export { AppAsideMenu };
