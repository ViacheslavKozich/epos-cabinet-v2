import { Outlet } from 'react-router-dom';
import { Grid } from '@mui/material'; 

import { AppContainer } from 'app/components/AppContainer';
import { AppHeader } from './AppHeader';
import { AppFooter } from './AppFooter';
import { AppAsideMenu } from './AppAsideMenu';


const Layout: React.FC = () => {

	return (<>
		<AppHeader/>
		
		<AppContainer>
			<Grid
				container wrap="nowrap" justifyContent="space-between"
				sx={{ mb: 8 }}
			>
				<Grid item minWidth="200px" sx={{ pr: 2 }}>
					<AppAsideMenu/>
				</Grid>

				<Grid item flexGrow={1} >
					<Outlet/>
				</Grid>
			</Grid>
		</AppContainer>
		
		<AppFooter/>
	</>)
}

export { Layout };
