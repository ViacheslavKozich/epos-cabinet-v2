import React from 'react'
import { notistack } from 'app/providers/NotistackProvider';
import { AppButton } from 'app/components/AppButton';


const HomePage: React.FC = () => {
	const showMessage = () => {
		notistack.warning('сообщение об успехе');
	}

	return (
		<div>
			<h2>HomePage</h2>
			<AppButton
				variant="main"
				sx={(theme) => ({	marginBottom: '10px' })}
				onClick={showMessage}
			>
				AppButton
			</AppButton>

			<AppButton
				variant="main"
				disabled
				sx={(theme) => ({	marginBottom: '10px' })}
				onClick={showMessage}
			>
				AppButton
			</AppButton>

			<AppButton
				variant="icon"
				sx={(theme) => ({	marginBottom: '10px' })}
				onClick={showMessage}
			>
				AppButton
			</AppButton>

			<AppButton
				variant="icon"
				disabled
				sx={(theme) => ({	marginBottom: '10px' })}
				onClick={showMessage}
			>
				AppButton
			</AppButton>

			<AppButton
				variant="text"
				sx={(theme) => ({	marginBottom: '10px' })}
				onClick={showMessage}
			>
				AppButton
			</AppButton>

			<AppButton
				variant="text"
				disabled
				sx={(theme) => ({	marginBottom: '10px' })}
				onClick={showMessage}
			>
				AppButton
			</AppButton>
		</div>
	);
}

export default HomePage;
