import React from 'react';
import { SplashScreenFallback } from 'app/providers';


const Suppliers: React.FC = () => {
	return (
		<React.Suspense fallback={SplashScreenFallback}>
			<h1>Suppliers</h1>
		</React.Suspense>
	);
}

export default Suppliers;
