// Types


/**
 * Конвертированный response, возвращаемый методами apiCalls. Два типа:
 * - при fulfilled { `ok`: true, `data`: data, `error`: null }
 * - при rejected { `ok`: false, `data`: null, `error`: error }
 * @property `ok` статус запроса
 * @property `data` response.data запроса
 * @property `error` ошибка запроса 
 */
export interface IConvertedResponse<D = any> {
	/** `true` при fulfilled response, `false` при rejected response */
	ok:    boolean;
	data:  null | D;
	error: string;
};
