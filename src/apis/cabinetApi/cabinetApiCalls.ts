import * as swagger from 'apis/cabinetApi/epos_cabinet_api';
import { cabinetAxios } from './cabinetAxios';
// Types
import { AxiosInstance, AxiosError, AxiosResponse } from 'axios';
import * as I from 'apis/cabinetApi/epos_cabinet_api/models';
import * as EI from './extraTypes';


class CabinetApiCalls {
	readonly axiosInstance: AxiosInstance;
	readonly HelpersApi;
	readonly InvoiceApi;
	readonly PayApi;
	readonly ReportApi;
	readonly ServiceProviderApi;
	readonly userApi;

	constructor(axiosInstance: AxiosInstance) {
		this.axiosInstance = axiosInstance;
		this.HelpersApi = new swagger.HelpersApi(undefined, undefined, this.axiosInstance);
		this.InvoiceApi = new swagger.InvoiceApi(undefined, undefined, this.axiosInstance);
		this.PayApi = new swagger.PayApi(undefined, undefined, this.axiosInstance);
		this.ReportApi = new swagger.ReportApi(undefined, undefined, this.axiosInstance);
		this.ServiceProviderApi = new swagger.ServiceProviderApi(undefined, undefined, this.axiosInstance);
		this.userApi = new swagger.UserManageApi(undefined, undefined, this.axiosInstance);
	}

	/**Метод преобразовывает fulfilled AxiosResponse для использования в приложении
	 * @param response AxiosResponse, возвращаемый api
	 * @returns преобразованный response
	 */
	private convertFulfilledResponse(response: AxiosResponse): EI.IConvertedResponse {
		const convertedResponse: EI.IConvertedResponse = {
			ok:    true,
			data:  response.data,
			error: ''
		};
		return convertedResponse;
	}

	/**Метод преобразовывает rejected AxiosResponse и подбирает текст ошибки
	 * @param error ошибка, пойманнаая при запросе на api
	 * @returns преобразованный response
	 */
	private convertRejectedResponse(error: any): EI.IConvertedResponse {
		let errorMessage: string;
		if (error.response) {
			const errResp = error.response;
			if (errResp.data?.code && errResp.data?.message) {
				errorMessage = `Ошибка ${errResp.data.code}: ${errResp.data.message}`;
			} else {
				errorMessage = `Ошибка ${errResp.status}: ${errResp.statusText}`;
			}
		} else if (error.request) {
			errorMessage = `Ошибка запроса на сервер`;
		} else {
			errorMessage = `Неизвестная ошибка в процессе выполнения запроса`;
		}

		const convertedResponse: EI.IConvertedResponse = {
			ok:    false,
			data:  null,
			error: errorMessage
		};
		return convertedResponse;
	}


	//! ========== Company API ==========
	/**[ POST ] /company                        | Регистрация организации в сервисе
	 * @param unp УНП компании
	 */
	// async createCompany(unp: string): Promise<EI.IConvertedResponse<I.CompanyProfile>> {
	// 	const requestBody = { unp };
	// 	try {
	// 		const createdCompany = await this.companyApi.companyPost(requestBody);
	// 		return this.convertFulfilledResponse(createdCompany);
	// 	} catch(error) {
	// 		return this.convertRejectedResponse(error);
	// 	}
	// }


	//! ========== Identity API ==========
	// модуль в tokenService

}

const eposCabinetApiCalls = new CabinetApiCalls(cabinetAxios.instance);

export { eposCabinetApiCalls };
