/* tslint:disable */
/* eslint-disable */
/**
 * EPOS cabinet API
 * Cabinet API сервиса E-POS
 *
 * OpenAPI spec version: v1
 * Contact: support-api@epos.by
 *
 * NOTE: This class is auto generated by the swagger code generator program.
 * https://github.com/swagger-api/swagger-codegen.git
 * Do not edit the class manually.
 */
/**
 * Код страны.  Используется 2 символа, которые определены в ISO 3166-1 alpha 2.
 * @export
 * @interface Locale
 */
export interface Locale {
    /**
     * Код страны.
     * @type {string}
     * @memberof Locale
     */
    code?: any | null;
    /**
     * Локализованное название страны.
     * @type {string}
     * @memberof Locale
     */
    name?: any | null;
}
