/* tslint:disable */
/* eslint-disable */
/**
 * EPOS cabinet API
 * Cabinet API сервиса E-POS
 *
 * OpenAPI spec version: v1
 * Contact: support-api@epos.by
 *
 * NOTE: This class is auto generated by the swagger code generator program.
 * https://github.com/swagger-api/swagger-codegen.git
 * Do not edit the class manually.
 */
/**
 * РТТ - розничная торговая точка.
 * @export
 * @interface RetailOutlet
 */
export interface RetailOutlet {
    /**
     * Уникальный числовой/строковый код РТТ.
     * @type {number}
     * @memberof RetailOutlet
     */
    code?: any | null;
    /**
     * 
     * @type {Address}
     * @memberof RetailOutlet
     */
    address?: any;
    /**
     * 
     * @type {BusinessCard}
     * @memberof RetailOutlet
     */
    businessCard?: any;
    /**
     * 
     * @type {RetailOutletMerchantInfo}
     * @memberof RetailOutlet
     */
    retailOutletMerchantInfo?: any;
    /**
     * 
     * @type {EripMCC}
     * @memberof RetailOutlet
     */
    eripMcc?: any;
    /**
     * 
     * @type {MCC}
     * @memberof RetailOutlet
     */
    mcc?: any;
    /**
     * Кассы РТТ
     * @type {Array&lt;CashBox&gt;}
     * @memberof RetailOutlet
     */
    cashBoxes?: any | null;
    /**
     * Описание РТТ.
     * @type {string}
     * @memberof RetailOutlet
     */
    descr?: any | null;
    /**
     * 
     * @type {NotifyParams}
     * @memberof RetailOutlet
     */
    notifyParams?: any;
    /**
     * 
     * @type {ServiceProviderStateEnum}
     * @memberof RetailOutlet
     */
    state?: any;
}
