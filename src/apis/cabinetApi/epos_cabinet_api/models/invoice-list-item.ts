/* tslint:disable */
/* eslint-disable */
/**
 * EPOS cabinet API
 * Cabinet API сервиса E-POS
 *
 * OpenAPI spec version: v1
 * Contact: support-api@epos.by
 *
 * NOTE: This class is auto generated by the swagger code generator program.
 * https://github.com/swagger-api/swagger-codegen.git
 * Do not edit the class manually.
 */
/**
 * Класс для отображения элемента списка счетов
 * @export
 * @interface InvoiceListItem
 */
export interface InvoiceListItem {
    /**
     * Внутренний идентификатор счета.
     * @type {string}
     * @memberof InvoiceListItem
     */
    id?: any;
    /**
     * Дата создания счета
     * @type {Date}
     * @memberof InvoiceListItem
     */
    addDateUTC?: any;
    /**
     * Дата оплаты счета
     * @type {Date}
     * @memberof InvoiceListItem
     */
    payDateUTC?: any | null;
    /**
     * Идентификатор родительской записи.
     * @type {string}
     * @memberof InvoiceListItem
     */
    parentId?: any | null;
    /**
     * Номер счета.
     * @type {string}
     * @memberof InvoiceListItem
     */
    number?: any | null;
    /**
     * Валюта инвойса. ISO 4217 трех символьный код валюты.
     * @type {string}
     * @memberof InvoiceListItem
     */
    currency?: any | null;
    /**
     * 
     * @type {ServiceProviderInfo}
     * @memberof InvoiceListItem
     */
    merchantInfo?: any;
    /**
     * 
     * @type {BillingInfo}
     * @memberof InvoiceListItem
     */
    billingInfo?: any;
    /**
     * 
     * @type {ShippingInfo}
     * @memberof InvoiceListItem
     */
    shippingInfo?: any;
    /**
     * Дата, с которой счет доступен на оплату (UTC).
     * @type {Date}
     * @memberof InvoiceListItem
     */
    dateInAirUTC?: any | null;
    /**
     * 
     * @type {PaymentDueTerms}
     * @memberof InvoiceListItem
     */
    paymentDueTerms?: any;
    /**
     * 
     * @type {PaymentRules}
     * @memberof InvoiceListItem
     */
    paymentRules?: any;
    /**
     * Общие условия счета.
     * @type {string}
     * @memberof InvoiceListItem
     */
    terms?: any | null;
    /**
     * Примечание к счету.
     * @type {string}
     * @memberof InvoiceListItem
     */
    note?: any | null;
    /**
     * 
     * @type {InvoiceStateEnum}
     * @memberof InvoiceListItem
     */
    state?: any;
    /**
     * Сумма по счету в валюте счета
     * @type {number}
     * @memberof InvoiceListItem
     */
    amount?: any;
    /**
     * Сумма к оплате по инвойсу на момент получения инвойса
     * @type {number}
     * @memberof InvoiceListItem
     */
    totalAmount?: any;
    /**
     * Сумма оплаты по счету в валюте счета
     * @type {number}
     * @memberof InvoiceListItem
     */
    amountPaid?: any;
    /**
     * Номер мем-ордера
     * @type {string}
     * @memberof InvoiceListItem
     */
    memOrderNum?: any | null;
    /**
     * Дата мем-ордера
     * @type {Date}
     * @memberof InvoiceListItem
     */
    memOrderDate?: any;
}
