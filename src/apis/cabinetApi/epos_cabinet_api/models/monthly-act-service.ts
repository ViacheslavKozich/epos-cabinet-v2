/* tslint:disable */
/* eslint-disable */
/**
 * EPOS cabinet API
 * Cabinet API сервиса E-POS
 *
 * OpenAPI spec version: v1
 * Contact: support-api@epos.by
 *
 * NOTE: This class is auto generated by the swagger code generator program.
 * https://github.com/swagger-api/swagger-codegen.git
 * Do not edit the class manually.
 */
/**
 * Структура сервиса для сверенной транзакции
 * @export
 * @interface MonthlyActService
 */
export interface MonthlyActService {
    /**
     * Код сервиса
     * @type {number}
     * @memberof MonthlyActService
     */
    code?: any;
    /**
     * Наименование сервиса
     * @type {string}
     * @memberof MonthlyActService
     */
    name?: any | null;
    /**
     * Количество сверенных транзакций
     * @type {number}
     * @memberof MonthlyActService
     */
    transactionQuantity?: any;
    /**
     * Оплаченная сумма клиентами
     * @type {number}
     * @memberof MonthlyActService
     */
    amount?: any;
    /**
     * Перечисленная ПУ сумма
     * @type {number}
     * @memberof MonthlyActService
     */
    transferAmount?: any;
    /**
     * Комиссия банка
     * @type {number}
     * @memberof MonthlyActService
     */
    bankComission?: any;
    /**
     * Комиссия агрегатора
     * @type {number}
     * @memberof MonthlyActService
     */
    aggregatorComission?: any;
    /**
     * Комиссия ЕРИП
     * @type {number}
     * @memberof MonthlyActService
     */
    eripComission?: any;
}
