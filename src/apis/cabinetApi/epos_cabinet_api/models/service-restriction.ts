/* tslint:disable */
/* eslint-disable */
/**
 * EPOS cabinet API
 * Cabinet API сервиса E-POS
 *
 * OpenAPI spec version: v1
 * Contact: support-api@epos.by
 *
 * NOTE: This class is auto generated by the swagger code generator program.
 * https://github.com/swagger-api/swagger-codegen.git
 * Do not edit the class manually.
 */
/**
 * Ограничения сервиса
 * @export
 * @interface ServiceRestriction
 */
export interface ServiceRestriction {
    /**
     * Минимальная сумма выставления по услуге
     * @type {number}
     * @memberof ServiceRestriction
     */
    minAmount?: any;
    /**
     * Максимальная сумма выставления по услуге
     * @type {number}
     * @memberof ServiceRestriction
     */
    maxAmount?: any;
    /**
     * Определеляет возможность проведения операции сторно по данной услуге
     * @type {boolean}
     * @memberof ServiceRestriction
     */
    canMakeStorno?: any;
    /**
     * Признак возможности проведения сторно на стороне ЕРИП
     * @type {boolean}
     * @memberof ServiceRestriction
     */
    eripStorno?: any;
    /**
     * Признак возможности проведения сторно на стороне агрегатора
     * @type {boolean}
     * @memberof ServiceRestriction
     */
    aggregatorStorno?: any;
}
