/* tslint:disable */
/* eslint-disable */
/**
 * EPOS cabinet API
 * Cabinet API сервиса E-POS
 *
 * OpenAPI spec version: v1
 * Contact: support-api@epos.by
 *
 * NOTE: This class is auto generated by the swagger code generator program.
 * https://github.com/swagger-api/swagger-codegen.git
 * Do not edit the class manually.
 */
/**
 * 
 * @export
 * @interface ServiceComission
 */
export interface ServiceComission {
    /**
     * 
     * @type {Comission}
     * @memberof ServiceComission
     */
    erip?: any;
    /**
     * 
     * @type {Comission}
     * @memberof ServiceComission
     */
    aggregator?: any;
}
