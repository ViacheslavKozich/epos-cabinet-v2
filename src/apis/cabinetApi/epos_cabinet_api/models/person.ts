/* tslint:disable */
/* eslint-disable */
/**
 * EPOS cabinet API
 * Cabinet API сервиса E-POS
 *
 * OpenAPI spec version: v1
 * Contact: support-api@epos.by
 *
 * NOTE: This class is auto generated by the swagger code generator program.
 * https://github.com/swagger-api/swagger-codegen.git
 * Do not edit the class manually.
 */
/**
 * Представитель
 * @export
 * @interface Person
 */
export interface Person {
    /**
     * Имя.
     * @type {string}
     * @memberof Person
     */
    firstName?: any | null;
    /**
     * Фамилия.
     * @type {string}
     * @memberof Person
     */
    lastName?: any | null;
    /**
     * Отчество.
     * @type {string}
     * @memberof Person
     */
    middleName?: any | null;
    /**
     * Полное имя
     * @type {string}
     * @memberof Person
     */
    fullName?: any | null;
}
