import { identityAxios } from './identityAxios';
import { getBaseUrl } from 'utils';
import { globalAppConfig } from 'appConfig';
// Types
import { AxiosInstance } from 'axios';


class IdentityApiCalls {
	readonly axiosInstance: AxiosInstance;

	constructor(axiosInstance: AxiosInstance) {
		this.axiosInstance = axiosInstance;
	}


	private getLoginRedirectURL(fromUrl: string): string {
		const authorizationUrl = `${globalAppConfig.appConfig.AUTH_URL}/connect/authorize`;
		const client_id = 'epos.cabinet';
		const response_type = 'code id_token';
		const redirect_uri = getBaseUrl() + 'auth';
		const scope = 'openid offline_access epos.cabinet.serviceprovider epos.cabinet.invoice email';
		const nonce = 'N' + Math.random() + Date.now();
		const state = fromUrl || '';

		const redirectUrlForLogin = authorizationUrl +
			'?' + 'client_id='     + encodeURI(client_id) +
			'&' + 'response_type=' + encodeURI(response_type) +
			'&' + 'redirect_uri='  + encodeURI(redirect_uri) +
			'&' + 'scope='         + encodeURI(scope) +
			'&' + 'nonce='         + encodeURI(nonce) +
			'&' + 'state='         + encodeURI(state);

		return redirectUrlForLogin;
	}

	private getLogoutRedirectURL(idToken: string): string {
		const authorizationUrl = `${globalAppConfig.appConfig.AUTH_URL}/connect/endsession`;
		const id_token_hint = idToken;
		const post_logout_redirect_uri = getBaseUrl();

		const redirectUrlForLogout = authorizationUrl +
			'?' + 'id_token_hint='            + id_token_hint +
			'&' + 'post_logout_redirect_uri=' + encodeURI(post_logout_redirect_uri);

		return redirectUrlForLogout;
	}


	//! ========== Login/logout to iii ==========
	redirectToAuthServerForLogin(fromUrl: string): void {
		const loginRedirectUrl = this.getLoginRedirectURL(fromUrl);
		window.location.assign(loginRedirectUrl);
	}

	redirectToAuthServerForLogout(idToken: string): void {
		const logoutRedirectUrl = this.getLogoutRedirectURL(idToken);
		window.location.assign(logoutRedirectUrl);
	}


	//! ========== UserInfo API ==========
	async getUserData(): Promise<any> {
		const axiosResponse = await this.axiosInstance.get('connect/userinfo');
		debugger
		return axiosResponse.data;
	}

}

const identityApiCalls = new IdentityApiCalls(identityAxios.instance);

export { identityApiCalls };
