import { AxiosFactory } from '../axiosFactory'
import { globalAppConfig } from 'appConfig';
import { tokensService } from 'app/auth/tokens/tokensService';


const identityAxios = new AxiosFactory({
	baseURL:        globalAppConfig.appConfig.AUTH_URL,
	getAccessToken: tokensService.getAccessTokenLocal,
	updateTokens:   tokensService.updateTokensAfterAccessTokenExpEnd,
});

export { identityAxios };
